from math import exp
from numpy import array, arange
from scipy.optimize import leastsq


class BuildLatticeVisitor:
    """
    called for each node when a lattice is being built, return properties of the next node
    need to implement the following method when build a lattice

    next(self,state,time)
      Parameters:
       - state,time : current state and column
      Return:
       - None : there is no next node
       - a list of (next_row,next_col)

    the next node could be in forwards direction(usually two nodes) or backwards direction(usually one node)
    """

    def __init__(self, T, lattice):
        self.T = T
        self.lattice = lattice

    def is_filled(self, time, state):
        """
        check if the specified node has already been populated
        """
        return (time, state) in self.lattice

def build_lattice(lattice, visitor, start_row=0, start_col=0):
    """
    build a lattice from node (start_row,start_col) by recursion on each next node returned by vistor.next()
    where visitor is an implementation of BuildLatticeVisitor
    """

    def populate(lattice, state, time):
        #print("populate (%d,%d)"%(state,time))
        next_node = visitor.next(state, time)
        if not next_node is None:
            for next_row, next_col in next_node:
                populate(lattice, next_row, next_col)

    populate(lattice, start_row, start_col)


def get_short_rate_lattice(r0, u, d, T):
    """
    return a T periods short rate lattice
    r0 : short rate from time 0~1
    u  : up going rate
    d  : down going rate
    """

    class ShortRateVisitor(BuildLatticeVisitor):
        """
        move along the lattice in forwards direction
        """

        def next(self, state, time):
            result = None

            if state < self.T and self.T > time >= state:
                result = []

                #going up case
                state_u = state + 1
                time_u = time + 1
                if not self.is_filled(state_u, time_u):
                    self.lattice[(state_u, time_u)] = self.lattice[(state, time)] * u
                    result.append((state_u, time_u))

                #going down case
                state_d = state
                time_d = time + 1
                if not self.is_filled(state_d, time_d):
                    self.lattice[(state_d, time_d)] = self.lattice[(state, time)] * d
                    result.append((state_d, time_d))

            return result

    lattice = {(0, 0): r0}
    build_lattice(lattice, ShortRateVisitor(T, lattice), 0, 0)
    return lattice


class LatticeBackwardsVisitor(BuildLatticeVisitor):
    """
    move along the lattice in backwards direction
    """

    def calc_value(self, state_u, time_u, state, time, state_next, time_next):
        pass

    def next(self, state, time):
        result = None

        if time >= 1:
            result = []

            while state < self.T and time >= state:
                state_u = state + 1
                time_u = time

                if state_u > time_u:
                    break

                state_next = state
                time_next = time - 1

                if state_next > time_next:
                    break

                if not self.is_filled(state_next, time_next):
                    self.lattice[(state_next, time_next)] = self.calc_value(state_u, time_u, state, time, state_next, time_next)
                    result.append((state_next, time_next))

                state += 1

        return result


class BondLatticesVisitor(LatticeBackwardsVisitor):
    def __init__(self, T, lattice, q, short_rate_lattice, coupon=0, face_value=100):
        super().__init__(T, lattice)
        self.q = q
        self.coupon = coupon
        self.face_value = face_value
        self.short_rate_lattice = short_rate_lattice

    def calc_value(self, state_u, time_u, state, time, state_next, time_next):
        """
        state_u,time_u       : state and time of up going value node
        state,time           : state and time of down going value node
        state_next,time_next : state and time of next node whose value id derived from up and down going nodes
        """
        u = self.lattice[(state_u, time_u)]
        d = self.lattice[(state, time)]
        return self.face_value * self.coupon + (u * self.q + d * (1 - self.q)) / (
            1 + self.short_rate_lattice[(state_next, time_next)])


def get_bond_lattice(short_rate_lattice, q, T, coupon=0, face_value=100):
    """ return a T periods Zero-Coupon Bond value lattice

        Args:
         short_rate_lattice -- lattice of short-rate
         q -- the probability of value goes up
         T -- maturity
         coupon -- coupon rate
         face_value -- face value of the bond
    """

    #the value on maturity of all status are the face value
    lattice = {(state, T):face_value * (1 + coupon) for state in range(T + 1)}

    build_lattice(lattice, BondLatticesVisitor(T, lattice, q, short_rate_lattice, coupon, face_value),0,T)
    return lattice


def get_option_lattice(short_rate_lattice, underlying_value_lattice, q, T, strike, is_call_option, is_american_option):
    """return an option lattice

       Args:
        underlying_value_lattice -- lattice of the underlying security value
        q -- the probability of value goes up
        T -- maturity
        strike -- option strike price
        is_call_option -- True if it is a call option else put option
        is_american_option -- True if it is an American option else European option
    """

    class AmericanOptionLatticesVisitor(BondLatticesVisitor):
        call_put_flag = 1 if is_call_option else -1

        def calc_value(self, state_u, time_u, state, time, state_next, time_next):
            """
            state_u,time_u       : state and time of up going value node
            state,time           : state and time of down going value node
            state_next,time_next : state and time of next node whose value id derived from up and down going nodes
            """
            eur_option_value = super().calc_value(state_u, time_u, state, time, state_next, time_next)
            return max(eur_option_value, self.call_put_flag * (underlying_value_lattice[(state_next, time_next)] - strike))

    call_put_flag = 1 if is_call_option else -1


    #final option pay-off
    lattice = {(state, T):max(0, call_put_flag * (underlying_value_lattice[(state, T)] - strike)) for state in range(T + 1)}

    build_lattice(lattice,
                  AmericanOptionLatticesVisitor(T, lattice, q, short_rate_lattice)
                  if is_american_option else
                  BondLatticesVisitor(T, lattice, q, short_rate_lattice),
                  0, T)

    return lattice


def bond_forward_price(short_rate_lattice, q, bond_coupon, bond_maturity, forward_maturity):
    #we count from t=0, so the bond's maturity is forward_maturity + bond_maturity
    modified_bond_maturity = forward_maturity + bond_maturity
    bond_lattice = get_bond_lattice(short_rate_lattice, q, modified_bond_maturity, bond_coupon)
    #display_lattice(bond_lattice,modified_bond_maturity)

    """
    Then build a ZCB from t=0 and matures on forward_maturity, whose face value is
    the modified maturity coupon bearing bond's value at t = forward_maturity.
    But because we are "converting" it to a ZCB, we need to deduct the coupon payment
    """

    #the value on maturity of all status are the face value
    forwad_zcb_lattice = {(state, forward_maturity):bond_lattice[(state, forward_maturity)] - 100 * bond_coupon for state in range(forward_maturity + 1)}

    build_lattice(forwad_zcb_lattice,
                  BondLatticesVisitor(forward_maturity, forwad_zcb_lattice, q, short_rate_lattice),
                  0, forward_maturity)
    #display_lattice(forwad_zcb_lattice,forward_maturity)
    z0 = forwad_zcb_lattice[(0, 0)]

    #also need Z0 of a ZCB whose maturity is same as the forward contract
    zcb_lattice = get_bond_lattice(short_rate_lattice, q, forward_maturity)

    #display_lattice(zcb_lattice,forward_maturity)
    zcb_z0 = zcb_lattice[(0, 0)]

    return z0 / (zcb_z0 / 100)


def bond_future_price(short_rate_lattice, q, bond_coupon, bond_maturity, future_maturity):
    class BondFutureVisitor(LatticeBackwardsVisitor):
        def calc_value(self, state_u, time_u, state, time, state_next, time_next):
            u = self.lattice[(state_u, time_u)]
            d = self.lattice[(state, time)]
            return u * q + d * (1 - q)

    #we count from t=0, so the bond's maturity is forward_maturity + bond_maturity
    modified_bond_maturity = future_maturity + bond_maturity
    bond_lattice = get_bond_lattice(short_rate_lattice, q, modified_bond_maturity, bond_coupon)
    #display_lattice(bond_lattice,modified_bond_maturity)

    """
    The value of the underlying security at t=future_maturity is same as
    the modified maturity coupon bearing bond's value at t = future_maturity.
    we need to deduct the coupon payment
    """

    lattice = {(state, future_maturity):bond_lattice[(state, future_maturity)] - 100 * bond_coupon for state in range(future_maturity + 1)}
    #note we don't discount it
    build_lattice(lattice, BondFutureVisitor(future_maturity, lattice), 0, future_maturity)
    #display_lattice(lattice,future_maturity)
    return lattice[(0, 0)]


def get_swap_lattice(short_rate_lattice, q, fixed_rate, maturity):
    """ pricing swap float rate

        Args:
         short_rate_lattice -- prevailing short rate lattice
         q -- the probability of value goes up
         fixed_rate -- the fixed rate
         maturity -- the last payment when the swap contract expires
    """

    class SwapLatticesVisitor(BondLatticesVisitor):
        def calc_value(self, state_u, time_u, state, time, state_next, time_next):
            """
            state_u,time_u       : state and time of up going value node
            state,time           : state and time of down going value node
            state_next,time_next : state and time of next node whose value id derived from up and down going nodes
            """
            u = self.lattice[(state_u, time_u)]
            d = self.lattice[(state, time)]
            short_rate = self.short_rate_lattice[(state_next, time_next)]
            payoff = short_rate - fixed_rate
            return (payoff + (u * self.q + d * (1 - self.q))) / (1 + short_rate)

    T = maturity - 1 #TODO
    #the last payment needs to be discounted
    lattice = {(state, T):(short_rate_lattice[(state, T)] - fixed_rate)/(1 + short_rate_lattice[(state, T)]) for state in range(maturity)}

    #display_lattice(short_rate_lattice,T,100)
    build_lattice(lattice, SwapLatticesVisitor(T, lattice, q, short_rate_lattice), 0, T)
    #display_lattice(lattice,T,1,"%8.4f"," " * 8,4)
    return lattice


def get_swaption_lattice(swap_lattice, short_rate_lattice, q, strike, maturity):
    lattice = {(state, maturity):max(0, swap_lattice[(state, maturity)] - strike) for state in range(maturity + 1)}

    build_lattice(lattice, BondLatticesVisitor(maturity, lattice, q, short_rate_lattice), 0, maturity)
    #display_lattice(lattice,maturity,1,"%8.4f"," " * 8,4)
    return lattice


def get_elementary_price_lattice(short_rate_lattice, T):
    """
    Populate an elementary price lattice
    Args:
        short_rate_lattice -- the short-rate lattice
        T -- how many years
    """

    class ElementaryPriceVisitor(BuildLatticeVisitor):
        """
        move along the lattice in forwards direction
        """

        def next(self, state, time):
            def calc(state, time):
                if time == state:
                    #diagonal line
                    k = state - 1
                    return self.lattice[(k, k)] / (2 * (1 + short_rate_lattice[(k, k)]))
                elif state == 0:
                    return self.lattice[(state, time - 1)] / (2 * (1 + short_rate_lattice[state, time - 1]))
                elif time == 0:
                    k = state - 1
                    return self.lattice[(k, 0)] / (2 * (1 + short_rate_lattice[(k, 0)]))
                else:
                    k = state - 1
                    s = time
                    return self.lattice[(k, s - 1)] / (2 * (1 + short_rate_lattice[(k, s - 1)])) + self.lattice[
                        (state, s - 1)] / (2 * (1 + short_rate_lattice[(state, s - 1)]))

            result = None

            if state < self.T and self.T > time >= state:
                result = []

                #going up case
                state_u = state + 1
                time_u = time + 1
                if not self.is_filled(state_u, time_u):
                    #print("calc u (%d,%d)"%(state_u, time_u))
                    self.lattice[(state_u, time_u)] = calc(state_u, time_u)
                    result.append((state_u, time_u))

                #going down case
                state_d = state
                time_d = time + 1
                if not self.is_filled(state_d, time_d):
                    #print("calc d (%d,%d)"%(state_d, time_d))
                    self.lattice[(state_d, time_d)] = calc(state_d, time_d)
                    result.append((state_d, time_d))

                state_d -= 1
                while state_d >= 0:
                    if not self.is_filled(state_d, time_d):
                        #print("calc d (%d,%d)"%(state_d, time_d))
                        self.lattice[(state_d, time_d)] = calc(state_d, time_d)
                        result.append((state_d, time_d))
                    state_d -= 1

            return result

    lattice = {(0, 0): 1}
    build_lattice(lattice, ElementaryPriceVisitor(T, lattice), 0, 0)
    return lattice


def calc_zcb_from_elementary_price_lattice(elementary_price_lattice, t):
    return sum([elementary_price_lattice[(x, t)] for x in range(t + 1)])


def calc_spot_rate_rom_elementary_price_lattice(elementary_price_lattice, zcb, t):
    """
    Calculate spot rate of the specified year from elementary price lattice
    Args:
        elementary_price_lattice -- the elementary price lattice
        t -- year
    """
    if not elementary_price_lattice is None:
        zcb = calc_zcb_from_elementary_price_lattice(elementary_price_lattice, t)
    return (1 / zcb) ** (1 / t) - 1


def get_BDT_Model_short_rate(a, b, T):
    """
    Calculate a short-rate lattice according to Black-Derman-Toy Model
    with b being fixed.
    Args:
      a -- list of a values
      b -- fixed b value
      T -- how many years
    """

    class BDTShortRateVisitor(BuildLatticeVisitor):
        """
        move along the lattice in forwards direction
        """

        def next(self, state, time):
            result = None

            if state < self.T and self.T > time >= state:
                result = []

                if not self.is_filled(state, time):
                    self.lattice[(state, time)] = a[time] * exp(b * state)

                #going up case
                state_u = state + 1
                time_u = time + 1
                result.append((state_u, time_u))

                #going down case
                state_d = state
                time_d = time + 1
                result.append((state_d, time_d))

            return result

    lattice = {}
    build_lattice(lattice, BDTShortRateVisitor(T, lattice), 0, 0)
    return lattice





def display_lattice(lattice, T, factor=1, format_str="%7.2f", blank=" " * 7, round_decimal=2):
    for state in range(T + 1, -1, -1):
        line = ""
        for time in range(T + 1):
            if (state, time) in lattice:
                line += format_str % (round(factor * lattice[(state, time)], round_decimal))
            else:
                line += blank
        print(line)


def BDT_Model_calibrate_a(a, b, T, target):
    """
    Calibrate parameter a in BDT model
    Args:
       a - a list of initial a values
       b - fixed b value
       T - how many years
       target - the target to meet
    """
    """
    #x20 slower than least-square
    def squared_error(a):
        l = get_BDT_Model_short_rate(a, b, T)
        e = get_elementary_price_lattice(l, T)
        spot = [calc_spot_rate_rom_elementary_price_lattice(e, 0, x) for x in range(1,T+1)]
        return sum((s - r) ** 2 for (s, r) in zip(spot, target))

    result = minimize(lambda x: squared_error(x), a, options = {'gtol': 1e-08})
    return result.x
    """
    #least square
    def peval(x, p):
        l = get_BDT_Model_short_rate(p, b, T)
        e = get_elementary_price_lattice(l, T)
        return array([calc_spot_rate_rom_elementary_price_lattice(e, 0, xx) for xx in x])

    def residuals(p, y, x):
        err = (array(target) - peval(x, p))
        return err

    x = arange(1, T + 1)
    y_meas = peval(x, a)
    return leastsq(residuals, a, args=(y_meas, x))[0]