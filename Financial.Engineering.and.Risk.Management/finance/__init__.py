__author__ = 'murphy'
from functools import reduce


def discount_rate(r, T):
    """
    return the discount rate when interest rate is r and the maturity is T
    """
    return 1.0 / ((1 + r) ** T)


def forward_rate(u, su, v, sv):
    """
    Compute forward rate from year u to year v
    where su is u-year spot rate
          sv is v-year spot rate
    """
    return (((1 + sv) ** v) / ((1 + su) ** u)) ** (1 / (v - u)) - 1


def forward_price(s0, rate, T, st=0, rate_t=0, t=0):
    """
    Compute the price of a forward contract matures on time T

    s0   : underlying value at time 0
    rate : interest rate (or from t0 to t if st>0)
    T    : maturity

    If st and rate_t and t is not 0, then compute the price at time t

    st     : underlying value at time t
    rate_t : interest rate from time t to T
    t      : time t
    """

    def forward_price_until_maturity(s0, rate, T):
        return s0 * ((1 + rate) ** T)

    if st == 0:
        return forward_price_until_maturity(s0, rate, T)
    else:
        F_t = forward_price_until_maturity(st, rate_t, T - t)
        F_0 = forward_price_until_maturity(s0, rate, T)
        return (F_t - F_0) / ((1 + rate) ** (T - t))


def perpetuity_price(payment, rate):
    """
    Compute the price of a perpetuity which pays payment
    rate : interest rate
    """
    return payment / rate


def irs_fixed_rate(rates):
    """
    return the fixed rate of an IRS
    rates : array of the float rates of all terms
    """
    T = len(rates)
    return (1 - discount_rate(rates[-1], T)) / reduce(lambda acc, x: acc + discount_rate(x[0], x[1]),
                                                      zip(rates, [i for i in range(1, T + 1)]), 0)


