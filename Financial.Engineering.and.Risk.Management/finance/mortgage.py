class LevelPayment:
    def __init__(self,loan,rate,term):
        """
        loan - the loan principle
        rate - loan annual rate
        term - term of loan in months
        """
        self.loan = loan
        self.rate = rate
        self.term = term

    def monthly_payment(self):
        c = self.rate/12
        #return (c*((1+c)**self.term)*self.term)/(((1+c)**self.term)-1)
        return (self.loan*c)/(1-(1+c)**(-self.term))

