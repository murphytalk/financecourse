# -*- coding: utf-8 -*-
import unittest

from numpy import pv, fv
from numpy.linalg import solve

from finance import *
from finance.time_structure import *
from finance.mortgage import *


class Week1(unittest.TestCase):
    def test_q1(self):
        """
        Lottery payments
        
        A major lottery advertises that it pays the winner $10 million.
        However this prize money is paid at the rate of $500,000 each year (with the first payment being immediate)
        for a total of 20 payments. What is the present value of this prize at 10% interest compounded annually?

        Report your answer in $millions, rounded to two decimal places.
        So, for example, if you compute the answer to be 5.7124 million dollars then you should submit an answer of 5.71. 
        """
        pmt = 500000
        #the first payment being immediate
        present_value = pmt
        present_value = (present_value + pv(0.1, 19, -pmt)) / 1000000
        #print("Week1.q1=%f"%present_value)
        self.assertEqual(round(present_value, 2), 4.68)

    def test_q2(self):
        """
        Sunk Costs
        
        A young couple has made a deposit of the first month's rent (equal to $1,000) on a 6-month apartment lease.
        The deposit is refundable at the end of six months if they stay until the end of the lease.

        The next day they find a different apartment that they like just as well, but its monthly rent is only $900.
        And they would again have to put a deposit of $900 refundable at the end of 6 months.

        They plan to be in the apartment only 6 months. Should they switch to the new apartment?
        Assume an (admittedly unrealistic!) interest rate of 12% per month compounded monthly.
        """
        rate = 0.12

        case1_rent = 1000
        case1_deposit = case1_rent

        case2_rent = 900
        case2_deposit = case2_rent

        months = 6

        #True - use PV; False - use FV
        use_pv = False

        if use_pv:
            #stay with case 1,compute PV all cash flow
            case1_pv = -case1_deposit + pv(rate, months, case1_rent)

            #discard case 1 deposit,switch to case 2
            case2_pv = -case1_deposit - case2_deposit + pv(rate, months, case2_rent)

            #print("case1 pv=%f,case2 pv=%f" % (case1_pv, case2_pv))
            #the cost of case1 is smaller,so should stay
            self.assertTrue(case1_pv > case2_pv)
        else:
            #stay with case 1, compute the FV of the cash flow by the end of 6th month
            case1_fv = fv(rate, months, case1_rent, case1_deposit)
            #discard case 1 deposit,switch to case 2
            case2_fv = fv(rate, months, case2_rent, case1_deposit + case2_deposit)
            #print("case 1 fv=%f,case 2 fv=%f" % (case1_fv, case2_fv))
            #The future value of case 1 is higher, so should stay
            self.assertTrue(case1_fv > case2_fv)

    def test_q3(self):
        """
        Relation between spot and discount rates

        Suppose the spot rates for 1 and 2 years are s1=6.3% and s2=6.9% with annual compounding.
        Recall that in this course interest rates are always quoted on an annual basis unless otherwise specified.
        What is the discount rate d(0,2)?
        Please submit your answer rounded to three decimal places.
        So, for example, if your answer is 0.4567 then you should submit an answer of 0.457.
        """
        s1 = 0.063
        s2 = 0.069
        years = 2
        discount = 1 / ((1 + s2) ** years)

        self.assertEqual(round(discount, 3), 0.875)

    def test_q4(self):
        """
        Relation between spot and forward rates

        Suppose the spot rates for 1 and 2 years are s1=6.3% and s2=6.9% with annual compounding.
        Recall that in this course interest rates are always quoted on an annual basis unless otherwise specified.
        What is the forward rate, f1,2 assuming annual compounding?

        Please submit your answer as a percentage rounded to one decimal place so, for example,
        if your answer is 8.789% then you should submit an answer of 8.8.

        """
        u = 1
        v = 2
        su = 0.063
        sv = 0.069
        f_12 = forward_rate(u, su, v, sv)
        #print("f_12=%f" % f_12)
        self.assertEqual(round(f_12, 3), 0.075)

    def test_q5(self):
        """
        Forward contract on a stock

        The current price of a stock is $400 per share and it pays no dividends.
        Assuming a constant interest rate of 8% per year compounded quarterly,
        what is the stock's theoretical forward price for delivery in 9 months?

        Please submit your answer rounded to two decimal places so for example,
        if your answer is 567.1234 then you should submit an answer of 567.12
        """
        #rate for 1 quarter
        rate = 0.08 / 4
        #maturity in quarter
        T = 9 / 3
        p = forward_price(400, rate, T)
        #print("price=%f" % p)
        self.assertEqual(round(p, 2), 424.48)

    def test_q6(self):
        """
        Bounds using different lending and borrowing rate

        Suppose the borrowing rate r_B=10% compounded annually.
        However, the lending rate (or equivalently, the interest rate on deposits) is only 8% compounded annually.
        Compute the difference between the upper and lower bounds on the price of an perpetuity
        that pays A=10,000$ per year.

        Please submit your answer rounded to the nearest dollar so if your answer is 23,456.789 then
        you should submit an answer of 23457.

        """
        payment = 10000
        diff = perpetuity_price(payment, 0.08) - perpetuity_price(payment, 0.1)
        self.assertEqual(diff, 25000)

    def test_q7(self):
        """
        Value of a Forward contract at an intermediate time

        Suppose we hold a forward contract on a stock with expiration 6 months from now.
        We entered into this contract 6 months ago so that when we entered into the contract,
        the expiration was T=1 year.
        The stock price$ 6 months ago was S0=100, the current stock price is 125 and the current interest rate is r=10%
        compounded semi-annually. (This is the same rate that prevailed 6 months ago.) What is the current value of our
        forward contract?

        Please submit your answer in dollars rounded to one decimal place so if your answer is 42.678 then
        you should submit an answer of 42.7.
        """
        #6 month rate
        rate = 0.1 / 2
        p = forward_price(100, rate, 2, 125, rate, 1)
        #print("F0=%f,Ft=%f,price=%f" % (F_0, F_t, p))
        self.assertEqual(p, 20.0)


class Week2(unittest.TestCase):
    def test_irs_price(self):
        rates = [0.05, 0.06, 0.075]
        self.assertEqual(round(irs_fixed_rate(rates), 4), 0.0737)


    def test_q1(self):
        """
        Term structure of interest rates and swap valuation

        Suppose the current term structure of interest rates, assuming annual compounding, is as follows:

        s1	  s2	s3	  s4	s5	  s6
        7.0%  7.3%	7.7%  8.1%	8.4%  8.8%

        What is the discount rate d(0,4)?
        (Recall that interest rates are always quoted on an annual basis unless stated otherwise.)
        Please submit your answer rounded to three decimal places.
        So for example, if your answer is 0.4567 then you should submit an answer of 0.457.

        """
        self.assertEqual(round(discount_rate(0.081, 4), 3), 0.732)

    def test_q2(self):
        """
        Swap Rates

        Suppose a 6-year swap with a notional principal of $10 million is being configured. What is the fixed rate of
        interest that will make the value of the swap equal to zero.
        (You should use the term structure of interest rates from Question 1.)

        Please submit your answer as a percentage rounded to two decimal places. So for example,
        if your answer is 4.567% or equivalently 0.04567, then you should submit an answer of 4.57.
        """
        rates = [0.07, 0.073, 0.077, 0.081, 0.084, 0.088]
        self.assertEqual(round(irs_fixed_rate(rates), 4), 0.0862)

    def test_q3(self):
        """
        Hedging using futures

        Suppose a farmer is expecting that her crop of oranges will be ready for harvest and sale as 150,000 pounds of
        orange juice in 3 months time. Suppose each orange juice futures contract is for 15,000 pounds of orange juice,
        and the current futures price is F0=118.65 cents-per-pound. Assuming that the farmer has enough cash liquidity
        to fund any margin calls, what is the risk-free price that she can guarantee herself.

        Please submit your answer in cents-per-pound rounded to two decimal places. So for example,
        if your answer is 123.456, then you should submit an answer of 123.47.
        """
        #perfect hedge
        F0 = 118.65
        price = F0
        self.assertEqual(F0, price)

    def test_q4(self):
        """
        Minimum variance hedge

        Suppose a farmer is expecting that her crop of grapefruit will be ready for harvest and sale as 150,000 pounds
        of grapefruit juice in 3 months time. She would like to use futures to hedge her risk but unfortunately
        there are no futures contracts on grapefruit juice. Instead she will use orange juice futures.

        Suppose each orange juice futures contract is for 15,000 pounds of orange juice and the current futures price
        is F0=118.65 cents-per-pound. The volatility, i.e. the standard deviation, of the prices of orange juice and
        grape fruit juice is 20% and 25%, respectively, and the correlation coefficient(normalized covariance) is 0.7.
        What is the approximate number of contracts she should purchase to minimize the variance of her payoff?

        Please submit your answer rounded to the nearest integer. So for example, if your calculations result in 10.78
        contracts you should submit an answer of 11.
        """
        sd_orange = 0.2
        sd_grape_fruit = 0.25
        #Corr(X,Y) x SD(X) x SD(Y) = COV(X,Y)
        #http://en.wikipedia.org/wiki/Covariance_and_correlation
        cor = 0.7
        cov = cor * sd_orange * sd_grape_fruit
        contracts = (cov / (sd_orange ** 2))
        self.assertEqual(round(contracts * 150000 / 15000), 9)

    def test_q5_6(self):
        """
        Call Options

        Consider a 1-period binomial model with R=1.02, S0=100, u=1/d=1.05. Compute the value of a European call option
        on the stock with strike K=102. The stock does not pay dividends.

        Please submit your answer rounded to two decimal places. So for example, if your answer is 3.4567 then you
        should submit an answer of 3.46.

        Call Options II

        When you construct the replicating portfolio for the option in the previous question how many dollars do you
        need to invest in the cash account?

        Please submit your answer rounded to three decimal places. So for example, if your answer is −43.4567 then you
        should submit an answer of −43.457.
        """
        u = 1.05
        d = 1 / u
        s0 = 100
        r = 1.02
        K = 102
        #exercise call option
        cu = s0 * u - K
        #does NOT exercise call option
        cd = 0

        #solve linear equations
        # u*s0*x+r*y = cu
        # d*s0*x+r*y = cd
        # Solve matrix a*b = c
        a = array([[u * s0, r], [d * s0, r]])
        c = array([cu, cd])
        b = solve(a, c)

        x = b[0]
        y = b[1]

        c0 = x * s0 + y
        #Q5
        self.assertEqual(2.04, round(c0, 2))
        #Q6
        self.assertEqual(-28.694, round(y, 3))


class Week4(unittest.TestCase):
    """
    Each of the following questions should be answered by building an n=10-period binomial model for the short-rate, r_i,j.
    The lattice parameters are: r0,0=5%, u=1.1, d=0.9 and q=1−q=1/2.
    """
    q = 0.5

    #example
    r0 = 0.06
    u = 1.25
    d = 0.9

    #quiz
    rr0 = 0.05
    uu = 1.1
    dd = 0.9

    def assert_rate(self, rate, factor, expected, rnd=2):
        self.assertEqual(round(rate * factor, rnd), expected)

    #
    # The followings are examples in spreadsheet class_resources-Term_Structure_Lattices
    # Each test corresponds to a sheet
    #

    def test_ZCB_Options(self):
        #Short-Rate Lattice
        lattice = get_short_rate_lattice(self.r0, self.u, self.d, 5)
        #display_lattice(lattice, 5, 100)
        expected = [3.54, 4.92, 6.83, 9.49, 13.18, 18.31]
        for i in range(6):
            self.assert_rate(lattice[(i, 5)], 100, expected[i])

        #ZCB values
        zcb_lattice = get_bond_lattice(lattice, self.q, 4)
        #display_lattice(zcb_lattice, 4)
        self.assert_rate(zcb_lattice[(0, 0)], 1, 77.22)

        #European option on ZCB
        eur_option_lattice = get_option_lattice(lattice, zcb_lattice, self.q, 2, 84, True, False)
        #display_lattice(eur_option_lattice, 2)
        self.assert_rate(eur_option_lattice[(0, 0)], 1, 2.97)

        #American option on ZCB
        us_option_lattice = get_option_lattice(lattice, zcb_lattice, self.q, 3, 88, False, True)
        #display_lattice(us_option_lattice, 3)
        self.assert_rate(us_option_lattice[(0, 0)], 1, 10.78)

    def test_BondForward_Futures(self):
        #Short-Rate Lattice
        short_rate_lattice = get_short_rate_lattice(self.r0, self.u, self.d, 5)

        #Bond forward
        forward = bond_forward_price(short_rate_lattice, self.q, 0.1, 2, 4)
        self.assertEqual(round(forward, 2), 103.38)

        #Bond futures
        future = bond_future_price(short_rate_lattice, self.q, 0.1, 2, 4)
        self.assertEqual(round(future, 2), 103.22)

    def test_Swaps_Swaptions(self):
        #Short-Rate Lattice
        short_rate_lattice = get_short_rate_lattice(self.r0, self.u, self.d, 5)

        swap_lattice = get_swap_lattice(short_rate_lattice, self.q, 0.05, 6)
        self.assertEqual(round(swap_lattice[(0, 0)], 4), 0.0990)

        swaption_lattice = get_swaption_lattice(swap_lattice, short_rate_lattice, self.q, 0, 3)
        self.assertEqual(round(swaption_lattice[(0, 0)], 4), 0.0620)

    def test_Elementary_Prices(self):
        #Elementary Prices
        T = 5
        short_rate_lattice = get_short_rate_lattice(self.r0, self.u, self.d, T)
        #display_lattice(short_rate_lattice, T, 100)
        elementary_price_lattice = get_elementary_price_lattice(short_rate_lattice, T + 1)
        #display_lattice(elementary_price_lattice, T+1)
        expected = [0.0119, 0.0686, 0.164, 0.2075, 0.1461, 0.0543, 0.0083]
        for i in range(7):
            self.assert_rate(elementary_price_lattice[(i, 6)], 1, expected[i], 4)

        #ZCB
        zcb_by_year = [94.34, 88.63, 82.91, 77.22, 71.59, 66.06]
        spot_rate_by_year = [6, 6.22, 6.45, 6.68, 6.91, 7.15]
        for t in range(1, T + 1 + 1):
            zcb = calc_zcb_from_elementary_price_lattice(elementary_price_lattice, t)
            self.assert_rate(100 * zcb, 1, zcb_by_year[t - 1])
            self.assert_rate(calc_spot_rate_rom_elementary_price_lattice(None, zcb, t), 100, spot_rate_by_year[t - 1])

    #
    # questions
    #
    def test_q1(self):
        """
        Compute the price of a zero-coupon bond (ZCB) that matures at time t=10 and that has face value 100.
        """
        T = 10
        short_rate_lattice = get_short_rate_lattice(self.rr0, self.uu, self.dd, T)
        zcb = get_bond_lattice(short_rate_lattice, self.q, T)
        #display_lattice(zcb, T)       
        self.assertEqual(round(zcb[(0, 0)], 2), 61.62)

    def test_q2(self):
        """
        Compute the price of a forward contract on the same ZCB of the previous question where the forward contract 
        matures at time t=4. 
        """
        T = 10
        short_rate_lattice = get_short_rate_lattice(self.rr0, self.uu, self.dd, T)
        forward_maturity = 4
        bond_maturity = T - forward_maturity
        forward = bond_forward_price(short_rate_lattice, self.q, 0, bond_maturity, forward_maturity)
        self.assertEqual(round(forward, 2), 74.88)

    def test_q3(self):
        """
        Compute the initial price of a futures contract on the same ZCB of the previous two questions. 
        The futures contract has an expiration of t=4.
        """
        T = 10
        short_rate_lattice = get_short_rate_lattice(self.rr0, self.uu, self.dd, T)
        future_maturity = 4
        bond_maturity = T - future_maturity
        future = bond_future_price(short_rate_lattice, self.q, 0, bond_maturity, future_maturity)
        #print("q3:%f,%f"%(future,round(future,2)))
        self.assertEqual(round(future, 2), 74.82)

    def test_q4(self):
        """
        Compute the price of an American call option on the same ZCB of the previous three questions. 
        The option has expiration t=6 and strike =80
        """
        T = 10
        short_rate_lattice = get_short_rate_lattice(self.rr0, self.uu, self.dd, T)
        zcb_lattice = get_bond_lattice(short_rate_lattice, self.q, T)
        us_option_lattice = get_option_lattice(short_rate_lattice, zcb_lattice, self.q, 6, 80, True, True)
        price = us_option_lattice[(0, 0)]
        #print("q4:%f,%f"%(price,round(price,2)))
        self.assertEqual(round(price, 2), 2.36)

    def test_q5_q6(self):
        """
        q5
        Compute the initial value of a forward-starting swap that begins at t=1, with maturity t=10 and a fixed rate of 4.5%. 
        (The first payment then takes place at t=2 and the final payment takes place at t=11 as we are assuming, as usual, 
        that payments take place in arrears.) 
        You should assume a swap notional of 1 million and assume that you receive floating and pay fixed.)
        """
        T = 10
        short_rate_lattice = get_short_rate_lattice(self.rr0, self.uu, self.dd, T)
        swap_lattice = get_swap_lattice(short_rate_lattice, self.q, 0.045, 11)
        """
        The above calculation assumes the swap payment starts from t=0.
        But in this question the payment starts from t=1, so the contract's value at t=0 should not include
        the payoff paid at the end of t=0. 
        """
        swap = (swap_lattice[(0, 1)] * self.q + swap_lattice[(1, 1)] * (1 - self.q)) / (1 + short_rate_lattice[(0, 0)])
        swap_lattice[(0, 0)] = swap
        #display_lattice(swap_lattice,T,1,"%8.4f"," " * 8,4)
        swap *= 1000000
        #print("q5:%f,%d"%(swap,round(swap,0)))
        self.assertEqual(round(swap, 0), 33374)

        """
        q6
        Compute the initial price of a swaption that matures at time t=5 and has a strike of 0. 
        The underlying swap is the same swap as described in the previous question with a notional of 1 million. 
        To be clear, you should assume that if the swaption is exercised at t=5 then the owner of the swaption will 
        receive all cash-flows from the underlying swap from times t=6 to t=11 inclusive. 
        (The swaption strike of 0 should also not be confused with the fixed rate of 4.5% on the underlying swap.)
        """
        swaption_lattice = get_swaption_lattice(swap_lattice, short_rate_lattice, self.q, 0, 5)
        swaption = swaption_lattice[(0, 0)] * 1000000
        #print("q6:%f,%d"%(swaption,round(swaption,0)))
        self.assertEqual(round(swaption, 0), 26311)


class Week5(unittest.TestCase):
    """
    The first two questions should be answered by building and calibrating a 10-period Black-Derman-Toy model for the short-rate, ri,j. 
    You may assume that the term-structure of interest rates observed in the market place is:

       Period    1       2       3       4       5       6       7        8       9    1   0
    Spot Rate    3.0%    3.1%    3.2%    3.3%    3.4%    3.5%    3.55%    3.6%    3.65%    3.7%
    
    As in the video modules, these interest rates assume per-period compounding so that, f
    or example, the market-price of a zero-coupon bond that matures in period 6 is Z60=100/(1+.035)6=81.35 assuming a face value of 100. 
    """
    q = 0.5

    def test_BDT(self):
        T = 14
        #initial a
        a = [0.05 for i in range(T)]
        #fixed value of b
        b = 0.005
        r = [7.3, 7.62, 8.1, 8.45, 9.2, 9.64, 10.12, 10.45, 10.75, 11.22, 11.55, 11.92, 12.2, 12.32]
        target = [x / 100 for x in r]
        calibrated_a = BDT_Model_calibrate_a(a, b, T, target)
        #print(calibrated_a)
        expected = [7.30, 7.92, 9.02, 9.44, 12.13, 11.72, 12.85, 12.57, 12.92, 15.20, 14.54, 15.64, 15.15, 13.45]
        for i in range(len(expected)):
            self.assertEqual(round(calibrated_a[i] * 100, 2), expected[i])

        swaption = self.swaption_value(a, b, target, T, 0.1165, 10, 2)
        #display_lattice(swaption, 2, 1, "%8.4f", " " * 8, 4)
        self.assertEqual(round(swaption, 6), 0.001339)

    def swaption_value(self, a, b, target, T, fixed_rate, swap_maturity, option_maturity):
        #fixed value of b
        calibrated_a = BDT_Model_calibrate_a(a, b, T, target)
        #print(calibrated_a)

        l = get_BDT_Model_short_rate(calibrated_a, b, T)
        swap_lattice = get_swap_lattice(l, self.q, fixed_rate, swap_maturity)
        swaption = get_swaption_lattice(swap_lattice, l, self.q, 0, option_maturity)
        return swaption[(0, 0)]

    def test_q1_q2(self):
        """
        Q1
        Assume b=0.05 is a constant for all i in the BDT model as we assumed in the video lectures. 
        Calibrate the ai parameters so that the model term-structure matches the market term-structure. 
        Be sure that the final error returned by Solver is at most 10−8. (This can be achieved by rerunning Solver multiple times if necessary, 
        starting each time with the solution from the previous call to Solver.

        Once your model has been calibrated, compute the price of a payer swaption with notional $1M that expires at time t=3 with an option strike of 0. 
        You may assume the underlying swap has a fixed rate of 3.9% and that if the option is exercised then cash-flows take place at times t=4,…,10. 
        (The cash-flow at time t=i is based on the short-rate that prevailed in the previous period, i.e. the payments of the underlying swap are made in arrears.)        
        """
        #observed market rates
        r = [3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.55, 3.6, 3.65, 3.7]
        target = [x / 100 for x in r]
        T = 10
        #initial a
        a = [0.05 for i in range(T)]

        Notional = 1000000
        #q1
        b = 0.05
        swaption = self.swaption_value(a, b, target, T, 0.039, 10, 3)
        #print(round(swaption * Notional))
        self.assertEqual(round(swaption * Notional), 4102)
        
        """
        Q2
        Repeat the previous question but now assume a value of b=0.1        
        """
        #q2
        b = 0.1
        swaption = self.swaption_value(a, b, target, T, 0.039, 10, 3)
        #print(round(swaption * Notional))
        self.assertEqual(round(swaption * Notional), 8097)

    def q3(self):
        """
        Construct a n=10-period binomial model for the short-rate, ri,j. 
        The lattice parameters are: r0,0=5%, u=1.1, d=0.9 and q=1−q=1/2. This is the same lattice that you constructed in Assignment 5.

        Assume that the 1-step hazard rate in node (i,j) is given by h_ij=ab^(j−i/2) where a=0.01 and b=1.01. 
        Compute the price of a zero-coupon bond with face value F=100 and recovery R=20%.
        """
        class HazardRateVisitor(BuildLatticeVisitor):            
            """
            move along the lattice in forwards direction
            """    
            def next(self, state, time):
                a = 0.01
                b = 1.01
                
                result = None
    
                if state < self.T and self.T > time >= state:
                    result = []
    
                    if not self.is_filled(state, time):
                        self.lattice[(state, time)] = a*b**(state-time/2)
    
                    #going up case
                    state_u = state + 1
                    time_u = time + 1
                    result.append((state_u, time_u))
    
                    #going down case
                    state_d = state
                    time_d = time + 1
                    result.append((state_d, time_d))
    
                return result
            
        class DefaultableBondLatticesVisitor(LatticeBackwardsVisitor):
            def __init__(self, T, lattice, R, q, short_rate_lattice,hazard_rate_lattice):
                super().__init__(T, lattice)
                self.q = q
                self.R = R
                self.short_rate_lattice = short_rate_lattice
                self.hazard_rate_lattice = hazard_rate_lattice
        
            def calc_value(self, state_u, time_u, state, time, state_next, time_next):
                """
                state_u,time_u       : state and time of up going value node
                state,time           : state and time of down going value node
                state_next,time_next : state and time of next node whose value id derived from up and down going nodes
                """
                u = self.lattice[(state_u, time_u)]
                d = self.lattice[(state, time)]
                r = self.short_rate_lattice[(state_next, time_next)]
                h = self.hazard_rate_lattice[(state_next, time_next)]
                
                non_default_value = self.q * (1-h) * u + (1 - self.q) * (1-h)*d
                recover_value = self.q*h*self.R + (1 - self.q)*h*self.R 
                
                return (non_default_value+recover_value)/(1+r)
            
        T = 10
        r0 = 0.05
        uu = 1.1
        dd = 0.9
        q = 0.5
        R = 20 #TODO
        
        short_rate_lattice = get_short_rate_lattice(r0, uu, dd, T)
        #display_lattice(short_rate_lattice, T, 1, "%8.4f", " " * 8, 4)
        
        #build hazard rate lattice
        hazard_rate_lattice = {}
        build_lattice(hazard_rate_lattice, HazardRateVisitor(T, hazard_rate_lattice), 0, 0)
        #display_lattice(hazard_rate_lattice, T, 1, "%8.4f", " " * 8, 4)
        
        face_value = 100
        #the value on maturity of all status are the face value
        lattice = {(state, T):face_value for state in range(T + 1)}
    
        build_lattice(lattice, DefaultableBondLatticesVisitor(T, lattice,R, q, short_rate_lattice,hazard_rate_lattice),0,T)
        #display_lattice(lattice, T)        
        #print(round(lattice[(0,0)],2))
        self.assertEqual(round(lattice[(0,0)],2), 57.22)


class Week6(unittest.TestCase):
    def test_level_monthly_payment(self):
        mortgage = LevelPayment(100000,8.125/100,360)
        #print("monthly payment=%f"%mortgage.monthly_payment())
        self.assertEqual(round(mortgage.monthly_payment(),2),742.50)

    def test_q1(self):
        mortgage = LevelPayment(400000,5/100,30*12)
        print("monthly payment=%f"%round(mortgage.monthly_payment(),2))

if __name__ == '__main__':
    unittest.main()

