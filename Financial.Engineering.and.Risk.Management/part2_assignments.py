# -*- coding: utf-8 -*-
import unittest


def choose(n, k):
    if k == 0:
        return 1
    else:
        return (n * choose(n - 1, k - 1)) / k


class Week2(unittest.TestCase):
    def get_loss(self):
        f = open('p2q3-loss.txt')
        loss = sorted([float(x) for x in f.readlines()])
        f.close()
        p = 0.9
        N = len(loss)
        return loss, p, N

    def test_q3(self):
        loss, p, N = self.get_loss()
        index = int(p * N)
        self.assertEqual(round(loss[index - 1], 2), 1.29)

    def test_q4(self):
        loss, p, N = self.get_loss()
        index = int(p * N)
        loss_sum = sum(loss[index - 1:])
        self.assertEqual(round(loss_sum / ((1 - p) * N), 2), 2.16)

    def calc_prob(self, n, r, p):
        return choose(n, r) * (p ** r) * ((1 - p) ** (n - r))

    def test_q5(self):
        self.assertEqual(round(sum([self.calc_prob(15, x, 0.5) for x in [12, 13, 14, 15]]), 4), 0.0176)

    def test_q6(self):
        P = sum([self.calc_prob(15, x, 0.5) for x in [14, 15]])
        M = 100
        PV = 1 - (1 - P) ** M
        self.assertEqual(round(PV, 4), 0.0477)


class Week4(unittest.TestCase):
    #default probabilities for credit #1~#20
    default_probabilities = [
        0.2,
        0.2,
        0.06,
        0.3,
        0.4,
        0.65,
        0.3,
        0.23,
        0.02,
        0.12,
        0.134,
        0.21,
        0.08,
        0.1,
        0.1,
        0.02,
        0.3,
        0.015,
        0.2,
        0.03,
    ]
    N = len(default_probabilities)

    def get_loss_probabilities(self):
        """
        Return a list of probabilities of #(0~N) loss in p_N
        """
        q = self.default_probabilities
        p = [0 for x in range(self.N + 1)]

        ## Initialize state
        ## Consider there is only the #1 credit
        ## probability of no loss
        p[0] = 1 - q[0]
        ## probability of #1 defaults
        p[1] = q[0]

        #then start to iterate from total 2 credits until N
        for i in range(2, self.N + 1):
            for j in range(i, 0, -1):
                ## default probability of #j is consisted of 2 parts
                ## 1)default probability of total previous j-1 credits joints with default probability of #j
                ## 2)default probability of
                p[j] = p[j - 1] * q[i - 1] + p[j] * (1 - q[i - 1])
            p[0] = p[0] * (1 - q[i - 1])

        return p

    def test_q1(self):
        """
        what is the probability of total 3 losses out of N
        """
        p = self.get_loss_probabilities()
        self.assertEqual(round(p[3], 3), 0.240)

    def test_q2(self):
        """
        What is the expected number of losses in the portfolio?
        """
        self.assertEqual(round(sum(self.default_probabilities), 2), 3.67)

        p = self.get_loss_probabilities()
        l = range(0, self.N + 1)
        self.assertEqual(round(sum([pp * ll for pp, ll in zip(p, l)]), 2), 3.67)

    def test_q3(self):
        """
        Compute the variance of the number of losses in the portfolio.

        see http://en.wikipedia.org/wiki/Bernoulli_distribution
        """
        p = self.default_probabilities
        variance = sum([x * (1 - x) for x in p])
        self.assertEqual(round(variance, 2), 2.54)

    def calc_expected_tranche_loss(self, loss_prob, low, up):
        expected_value = 0
        i = low
        loss = 0
        while i < up:
            expected_value += loss_prob[i] * loss
            i += 1
            loss += 1
        #will loss up to value [loss] if [up] and more default
        expected_value += sum(loss_prob[up:]) * loss
        return expected_value

    def test_q4(self):
        """
        What is the expected tranche loss in the tranche with lower and upper attachment points of 0 and 2, respectively?
        """
        p = self.get_loss_probabilities()
        self.assertEqual(round(self.calc_expected_tranche_loss(p, 0, 2), 2), 1.91)

    def test_q5(self):
        """
        What is the expected tranche loss in the tranche with lower and upper attachment points of 2 and 4, respectively?
        """
        p = self.get_loss_probabilities()
        self.assertEqual(round(self.calc_expected_tranche_loss(p, 2, 4), 2), 1.28)

    def test_q6(self):
        """
        What is the expected tranche loss in the tranche with lower and upper attachment points of 4 and 20, respectively?
        """
        total_losses = sum(self.default_probabilities)
        p = self.get_loss_probabilities()

        p_0_2 = self.calc_expected_tranche_loss(p, 0, 2)
        p_2_4 = self.calc_expected_tranche_loss(p, 2, 4)
        self.assertEqual(round(total_losses - p_0_2 - p_2_4, 2), 0.47)


if __name__ == '__main__':
    unittest.main()

